const defaultProfilePicture = 'https://via.placeholder.com/50x50.png';

const customStyle = {
    boxShadow : "inset -1px 0 0 rgba(0, 0, 0, 0.1)",
}; 

function getProfilePicture(imageURL){
    try{
        new URL(imageURL);
        return imageURL;
    } catch {
        return defaultProfilePicture;
    };
}

export default function SideBar({user:{id, name, image}}){
    return <div className="col-3 p-3 bg-light vh-100 overflow-scroll" style={customStyle}>
        <p>
            Benvenuto {name} <img src={getProfilePicture(image)} width={50} height={50}></img>, al tuo profilo abbiamo associato il seguente id: {id}. 
        </p>
    </div>
}