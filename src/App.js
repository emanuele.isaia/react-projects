import './App.css';
import Main from './components/Main';
import SideBar from './components/Sidebar';

const user = {
  name: "Emanuele",
  id: 1,
  image: "https://github.com/EmanueleIsaia.png"
};

export default function App(){
  return (
  <div className='container-flui'>
    <div className='row'>
      <SideBar user={user}></SideBar>
      <Main></Main>
    </div>
  </div>
  )
}
